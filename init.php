<?php
/*
Plugin Name: BTG Properties Search
Plugin URI: http://properties.begbies-traynor.com/
Description: Provides a widget and admin interface(s) for searching properties.
Version: 1.0
Author: Phil Kershaw
Author URI: http://blog.philkershaw.me/
*/

#--------------------------------------------------------------------------
#
#   BTG Property Search
#
#   Phil Kershaw, phil.kershaw@begbies-traynor.com
#	Begbies Traynor Group Plc.
#
#	TODO: 
#	-	
#
#	CHANGE LOG:
#	-	
#
#--------------------------------------------------------------------------
// ini_set('display_errors', '1');
// error_reporting(E_ALL);

# set plugin version
$version = '1.0';
if ($wp_version = get_option('BTG_property_search_version'))
{
	if ($wp_version != $version)
		update_option('BTG_property_search_version', $version);
}
else
{
	add_option('BTG_property_search_version', $version);
}

# set plugin path
define( 'BTG_SEARCH_PATH', plugin_dir_path(__FILE__) );

# add property types to WordPress database
$types = array('Commercial', 'Residential', 'Land');
update_option('BTG_property_search_types', $types);

# include classes
require_once 'classes/base.class.php';
require_once 'classes/widget.class.php';
require_once 'classes/admin.class.php';
require_once 'classes/authoring.class.php';

# register search widget
add_action( 'widgets_init', function(){
	return register_widget( 'BTGPropertySearch\Widget' );
});

# add css to front-end
add_action( 'wp_head', function(){
	echo '<link rel="stylesheet" type="text/css" media="all" id="btg-property-search" href="'. plugin_dir_url(__FILE__) .'assets/css/style.css">';
});

# add css to back-end
add_action( 'admin_head', function(){
	echo '<link rel="stylesheet" type="text/css" media="all" id="btg-property-search" href="'. plugin_dir_url(__FILE__) .'assets/css/style.css">';
});

# add admin menu for settings page
$admin_class = new BTGPropertySearch\Admin;
add_action( 'admin_menu', array($admin_class, 'menu') );

# replace theme search template with plugin custom
add_filter('template_include', array($admin_class, 'search_template'));

# add meta box in editing/authoring page
$authoring_class = new BTGPropertySearch\Authoring;
add_action( 'add_meta_boxes', array($authoring_class, 'geo_area_meta_box'));

# add javascript file to <head> of front-end
wp_register_script('jquery-validate', 'http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js');
wp_enqueue_script('btg-property-search', plugin_dir_url(__FILE__).'assets/js/btg-property-search.js', array('jquery', 'jquery-ui-core', 'jquery-ui-autocomplete', 'jquery-validate'));

# register and enqueue jquery ui style(s)
wp_register_style( 'jquery-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/smoothness/jquery-ui.css', true);
wp_enqueue_style( 'jquery-style' );

# add global javascript variables
wp_localize_script('btg-property-search', 'BTGPropertySearch', array(
		'ajaxurl' => admin_url('admin-ajax.php'),
	));

# register function appointed to proccess the ajax request submitted from the search box
$widget_class = new BTGPropertySearch\Widget;
add_action( 'wp_ajax_nopriv_btg-property-search-live-results', array($widget_class, 'ajaxProcess') );
add_action( 'wp_ajax_btg-property-search-live-results', array($widget_class, 'ajaxProcess') );

jQuery(document).ready(function() 
{
    if(jQuery('.btg_property_search_phrase').length != 0)
    {
        var input = jQuery('#s');
        var cache = {},
                    lastXhr;

        jQuery('.btg_property_search_phrase').autocomplete({
            minLength: 2,
            source: function( request, response ) {
                var term = request.term;
                var type = jQuery('#btg_property_search_type :checked').val();
                if (term in cache){
                    if (type in cache[term]){
                        response( cache[term][type] );
                        return;
                    }
                }

                lastXhr = jQuery.post(
                    BTGPropertySearch.ajaxurl,
                    {
                        action : 'btg-property-search-live-results',
                        query  : term,
                        type   : type
                    },
                    function (data, status, xhr){
                        if (!cache[term])
                            cache[term] = {};

                        cache[term][type] = data;
                        if (xhr === lastXhr){
                            response( data );
                        }
                });
            },
            select: function(event, ui){
                if ( ui.item.value === 'No Properties Found.')
                {
                    jQuery('.btg_property_search_phrase').val('');
                }
                else
                {
                    ui.item.value = ui.item.value.replace(
                            /<\s*\/?\s*span\s*.*?>|\s\(\d+\)/ig,
                            ''
                    );
                    jQuery('.btg_property_search_phrase').val(ui.item.value);
                }
                jQuery('#btg_property_search').submit();
                return false;
            },
            focus: function(event, ui){
                if ( ui.item.value === 'No Properties Found.')
                {
                    jQuery('.btg_property_search_phrase').val('');
                }
                else
                {
                    ui.item.value = ui.item.value.replace(
                            /<\s*\/?\s*span\s*.*?>|\s\(\d+\)/ig,
                            ''
                    );
                    jQuery('.btg_property_search_phrase').val(ui.item.value);
                }
            }
        })
        .data( "autocomplete" )._renderItem = function( ul, item ) {
            return jQuery( "<li></li>" )
                .data( "item.autocomplete", item )
                .append( jQuery("<a></a>").html( item.label ))
                .appendTo( ul );
        };
    }

    if (jQuery("#post").length != 0)
    {
        jQuery("#post").validate();
    }
});
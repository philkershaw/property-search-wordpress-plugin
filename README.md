Property Search WordPress Plugin
================================

Briefly consists of:

- A widget containing a 'type' radio group and search box that takes advantage of jQuery UI's autocomplete functionality;
- A meta box in the authoring page to select the area and type;
- An admin page to manage areas.

It's kinda bespoke to what I required at the time but providing you give me credit and list my author URI in your derivative of this plugin you are free to use this code.

test
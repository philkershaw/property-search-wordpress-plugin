<?php

namespace BTGPropertySearch;

class Authoring extends Base{

	public $meta;

	public function __construct()
	{
		parent::__construct();
		add_action('save_post', array($this, 'geo_area_save'));
	}

	public function geo_area_meta_box()
	{
		add_meta_box('btg_property_search', __( 'Geographical Area' ), array($this, 'geo_area_inner'), 'sale');
	}
	/**
	 * geo_area_inner()
	 * Constructs the HTML for the Geographical Area meta box.
	 */
	public function geo_area_inner()
	{
		global $post;
		$this->meta = get_post_meta($post->ID, 'btg_property_search_areas', true);
		$this->format_areas();
		$type = get_post_meta($post->ID, 'btg_property_search_type', true);
		$types = get_option('BTG_property_search_types');

		wp_nonce_field(plugin_basename(__FILE__), 'btg_property_search_nonce');
?>
		<select id="btg_property_search_select" name="region" class="required">
			<option value="">Region...</option>
			<?php echo $this->regions; ?>
		</select><br />
		<select id="btg_property_search_select" name="county" class="required">
			<option value="">County...</option>
			<?php echo $this->counties; ?>
		</select><br />
		<select id="btg_property_search_select" name="town-city" class="required">
			<option value="">Town/City...</option>
			<?php echo $this->towns_cities; ?>
		</select><br />
		<select id="btg_property_search_select" name="type" class="required">
			<option value="">Type...</option>
<?php
			foreach($types as $t)
			{
				$check = ($t == $type) ? 'selected="selected"' : '';
				echo '<option value="'.$t.'" '.$check.'>'.$t.'</option>';
			}
?>
		</select>
<?php
	}
	/**
	 * geo_area_save()
	 * instructs WordPress on what to do with our extra info when saving a post.
	 * 
	 * @param int $post_id The id of the post currently being edited.
	 */
	public function geo_area_save($post_id)
	{
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
			return;

		if ( !wp_verify_nonce( $_POST['btg_property_search_nonce'], plugin_basename( __FILE__ ) ) )
			return;

		if ( 'sale' == $_POST['post_type'] ) 
		{
			if ( !current_user_can( 'edit_page', $post_id ) )
				return;
		}
		else
		{
			if ( !current_user_can( 'edit_post', $post_id ) )
				return;
		}

		$text = $_POST['region'].', '.$_POST['county'].', '.$_POST['town-city'];
		update_post_meta($post_id, 'btg_property_search_areas', $text);
		update_post_meta($post_id, 'btg_property_search_type', $_POST['type']);
	}
}


<?php

namespace BTGPropertySearch;

class Admin extends Base {

	public $before_page;
	public $after_page;
	public $hidden_field_name = 'btg_submit_hidden';

	public function __construct()
	{
		parent::__construct();
		$this->format_areas();
		$this->before_page();
		$this->after_page();
	}
	/**
	 * before_page()
	 * Custom function to enable adding of customisation to the beginning of the admin page.
	 * 
	 */
	public function before_page()
	{
		$this->before_page = '<div class="wrap">
								<div id="icon-options-general" class="icon32"><br></div>
								<h2>BTG Property Search Settings</h2>';
		if( isset($_POST[ $this->hidden_field_name ]) && $_POST[ $this->hidden_field_name ] == 'Y' )
		{
			$this->save();
		}
		// print_r($this->areas);
	}
	/**
	 * after_page()
	 * Custom function to enable adding of customisation to the end of the admin page.
	 * 
	 */
	public function after_page()
	{
		$this->after_page = '</div>';
	}
	/**
	 * menu()
	 * Add admin menu option.
	 */
	public function menu()
	{
		//create new top-level menu
		add_menu_page('BTG Property Search Settings', 'BTG Property Search Settings', 'administrator', __FILE__, array($this,'page'));
	}
	/**
	 * page()
	 * Constructs the settings page.
	 */
	public function page()
	{
    	$data_field_name = 'btg_favorite_colour';
    	echo $this->before_page;
		?>

<form name="form1" method="post" action="">
	<input type="hidden" name="<?php echo $this->hidden_field_name; ?>" value="Y">

	<p><?php _e("Region:"); ?> 
		<input type="text" name="region_txt" value="" placeHolder="e.g. North West" size="40"> OR 
		<select name="region_sel"><option value="0">Select Region...</option><?php echo $this->regions; ?></select>
	</p><hr />
	<p><?php _e("County:"); ?> 
		<input type="text" name="county_txt" value="" placeHolder="e.g. Greater Manchester" size="40"> OR 
		<select name="county_sel"><option value="0">Select County...</option><?php echo $this->counties; ?></select>
	</p><hr />
	<p><?php _e("Town/City:"); ?> 
		<input type="text" name="town-city_txt" value="" placeHolder="e.g. Manchester" size="40"> OR 
		<select name="town-city_sel"><option value="0">Select Town/City...</option><?php echo $this->towns_cities; ?></select>
	</p><hr />

	<p class="submit">
		<input type="submit" name="Submit" class="button-primary" value="<?php esc_attr_e('Add Area') ?>" />
	</p>
</form>
<table width="90%">
	<tr>
		<th align="left">Region</th>
		<th align="left">County</th>
		<th align="left">Town/City</th>
	</tr>
	<?php 
		if ($this->areas)
		{
			foreach($this->areas as $region => $counties)
			{
				foreach($counties as $county => $towns_cities)
				{
					foreach($towns_cities as $town_city)
					{
						echo "<tr>";
						echo "<td>{$region}</td>";
						echo "<td>{$county}</td>";
						echo "<td>{$town_city}</td>";
						echo "</tr>";
					}
				}
			}
		}
		?>
</table>
		<?php
		echo $this->after_page;
	}
	/**
	 * save()
	 * Save settings.
	 */
	public function save()
	{
		$new_region    = ($_POST['region_sel'] == '0') ? $_POST['region_txt'] : $_POST['region_sel'];
		$new_county    = ($_POST['county_sel'] == '0') ? $_POST['county_txt'] : $_POST['county_sel'];
		$new_town_city = ($_POST['town-city_sel'] == '0') ? $_POST['town-city_txt'] : $_POST['town-city_sel'];
		if($new_region == '' OR $new_county == '' OR $new_town_city == '')
		{
			$this->before_page .= '<div class="error"><p><strong>'.__("All fields are required.").'</strong></p></div>';
			return;
		}
		if(is_null($this->areas))
		{
			$this->areas = array();
			$this->areas[$new_region][$new_county] = array($new_town_city);
		}
		elseif(!isset($this->areas[$new_region][$new_county]))
		{
			$this->areas[$new_region][$new_county] = array($new_town_city);
		}
		elseif(!in_array($new_town_city, $this->areas[$new_region][$new_county]))
		{
			array_push($this->areas[$new_region][$new_county], $new_town_city);
		}
		else
		{
			$this->before_page .= '<div class="error"><p><strong>'.__("The area you entered already exists.").'</strong></p></div>';
			return false;
		}
		// print_r($this->areas);
		update_option('btg_property_search_areas', $this->areas);
		$this->before_page .= '<div class="updated"><p><strong>'.__("Settings saved.").'</strong></p></div>';
	}
	/**
	 * Over-ride theme search template
	 */
	public function search_template($template){
	    global $wp_query;
	    if (!$wp_query->is_search)
	        return $template;

	    return BTG_SEARCH_PATH . 'templates/search_results.php';
	}
}
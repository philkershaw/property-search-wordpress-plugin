<?php

namespace BTGPropertySearch;

class Widget extends \WP_Widget
{
	function __construct()
	{
			parent::__construct( 'btg_property_search', 'BTG Property Search Widget' );
	}
	/**
	* Front-end display of widget.
	*
	* @see WP_Widget::widget()
	*
	* @param array $args     Widget arguments.
	* @param array $instance Saved values from database.
	*/
	public function widget( $args, $instance )
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		$types = get_option('BTG_property_search_types');
		$admin_only = $instance['admin_only'];

		if ($admin_only)
		{
			if (!current_user_can('level_10'))
				return;
		}

		echo $before_widget;
		if ( ! empty( $title ) )
			echo $before_title . $title . $after_title;
		?>
		<form id="btg_property_search">
		    <input class="btg_property_search_phrase required" id="s" name="s" type="text" placeHolder="Type a search e.g. Manchester OR North West" maxlength="150" />
		    <div id="btg_property_search_type">
		<?php
			foreach($types as $t)
			{
				$check = ($t == 'Commercial') ? 'checked' : '';
				echo '<input type="radio" name="btg_property_search_type" id="type" value="'.$t.'" '.$check.'> '.$t;
			}
		?>
			</div>

			<input class="btg_property_search_button" name="<?php echo $this->get_field_id( 'button' ); ?>" id="<?php echo $this->get_field_id( 'button' ); ?>" value="Search" type="submit">
		</form>
		<?php

		echo $after_widget;
	}
	/**
	* Sanitize widget form values as they are saved.
	*
	* @see WP_Widget::update()
	*
	* @param array $new_instance Values just sent to be saved.
	* @param array $old_instance Previously saved values from database.
	*
	* @return array Updated safe values to be saved.
	*/
	public function update( $new_instance, $old_instance )
	{
		$instance = array();
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['admin_only'] = strip_tags( $new_instance['admin_only'] );
		return $instance;
	}
	/**
	* form
	* Constructs the form for the admin widget options
	*
	* @see WP_Widget::form()
	*
	* @param array $instance Previously saved values from database.
	*/
	public function form( $instance )
	{
		if ( isset( $instance[ 'title' ] ) )
		{
			$title = $instance[ 'title' ];
		}
		else
		{
			$title = __( 'Title', 'text_domain' );
		}
		$admin_only = $instance['admin_only'];
		?>
			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" /><br /><br />
				<label for="<?php echo $this->get_field_id( 'admin_only' ); ?>"><?php _e( 'Admin Only:' ); ?></label>
				<input type="checkbox" id="<?php echo $this->get_field_id( 'admin_only' ); ?>" name="<?php echo $this->get_field_name( 'admin_only' ); ?>" <?php echo ($admin_only) ? 'checked="checked"' : '' ?>
			</p>
		<?php
	}
	public function ajaxProcess()
	{
		// get the submitted parameters
		$query        = $_POST['query'];
		$type         = $_POST['type'];
		$areas        = get_option('btg_property_search_areas');
		$areas        = $this->flatten_array($areas);
		$result_areas = array();
	    foreach ($areas as $key => $area)
	    {
			if (stristr($area, $query))
			{
				array_push($result_areas, $area);
			}
	    }
	    $result_areas = $this->properties_exist($result_areas, $type);
	    $result_areas = $this->add_highlights($result_areas, $query);
	    // generate the response
	    $response = json_encode($result_areas);
	    // response output
	    header( "Content-Type: application/json" );
	    echo $response;
	    // IMPORTANT: don't forget to "exit"
	    exit;
	}
	/**
	* flatten_array()
	* flattens the area into a single level array.
	* 
	* @param array $areas multidimentional array of areas.
	* 
	* @return array $flat_areas single dimention array of areas.
	*/
	public function flatten_array($areas)
	{
		$flat_areas = array();
		foreach ($areas as $region => $counties)
		{
			foreach ($counties as $county => $towns_cities)
			{
				foreach ($towns_cities as $town_city)
				{
					$area = $region.', '.$county.', '.$town_city;
					array_push($flat_areas, $area);
				}
			}
		}
		return $flat_areas;
	}
	/**
	* properties_exist()
	* checks the wordpress database for properties that exist for the given area(s).
	* 
	* @param array $data array holding areas string(s) to be checked against the database.
	* 
	* @return array $data_with_count array containing the original param with counts appended.
	*/
	public function properties_exist($data, $type)
	{
		global $wpdb;

		foreach($data as $key => $area)
		{
			$properties = $wpdb->get_results("
				SELECT post_id
				FROM {$wpdb->postmeta}
				WHERE meta_key = 'btg_property_search_areas'
				AND meta_value = '{$area}'"
			);
			if (count($properties) > 0)
			{
				$total = 0;
				foreach($properties as $property)
				{
					$id = $property->post_id;
					$property_type = $wpdb->get_results("
						SELECT meta_value
						FROM {$wpdb->postmeta}
						WHERE meta_key = 'btg_property_search_type'
						AND post_id = {$id}"
					);
					if ($property_type[0]->meta_value == $type)
						$total++;
				}
				if ($total > 0)
				{
					$data[$key] = "{$area} ({$total})";
				}
				else
				{
					unset($data[$key]);
				}
			}
			else
			{
				unset($data[$key]);
			}
		}
		if (count($data) > 0)
			return $data;

		return array("No Properties Found.");
	}
	/**
	* add_highlights()
	* Adds a span tag to highlight the search term in the area result.
	* 
	* @param array $areas an array of the areas with properties matching the search term
	* @param string $term the original search term.
	* 
	* @return array $areas the originally passed array with the highlights applied.
	*/
	public function add_highlights($areas, $term)
	{
		$rtn = array();
		foreach ($areas as $key => $area)
		{
			$str = str_ireplace($term, '<span style="color: green;">'.$term.'</span>', $area);
			array_push($rtn, $str);
		}
		return $rtn;
	}
}
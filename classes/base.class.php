<?php

namespace BTGPropertySearch;

class Base {

	public $areas;
	public $regions = '';
	public $counties = '';
	public $towns_cities = '';
	public $regions_array = array();
	public $counties_array = array();
	public $towns_cities_array = array();

	public function __construct()
	{
		if (get_option('btg_property_search_areas'))
		{
			$this->areas = get_option('btg_property_search_areas');
			$this->sort_areas();
		}
	}
	/**
	 * format_areas()
	 * Crawls through the areas array and splits the content out into regions, counties and town/cities. Then applies select/option HTML.
	 */
	public function format_areas()
	{
		if (isset($this->meta) AND $this->meta != '')
		{
			$area = explode(', ', $this->meta);
			// print_r($area);
			if (isset($this->areas))
			{
				// foreach ($this->areas as $region => $counties)
				// {
				// 	$selected = ($region == $area[0]) ? 'selected="selected"' : '';
				// 	$this->regions .= '<option value="'.$region.'" '.$selected.'>'.$region.'</option>';
				// 	foreach($counties as $county => $towns_cities)
				// 	{
				// 		$selected = ($county == $area[1]) ? 'selected="selected"' : '';
				// 		$this->counties .= '<option value="'.$county.'" '.$selected.'>'.$county.'</option>';
				// 		foreach($towns_cities as $town_city)
				// 		{
				// 			$selected = ($town_city == $area[2]) ? 'selected="selected"' : '';
				// 			$this->towns_cities .= '<option value="'.$town_city.'" '.$selected.'>'.$town_city.'</option>';
				// 		}
				// 	}
				// }
				foreach($this->regions_array as $region)
				{
					$selected = ($region == $area[0]) ? 'selected="selected"' : '';
					$this->regions .= '<option value="'.$region.'" '.$selected.'>'.$region.'</option>';
				}

				foreach($this->counties_array as $county)
				{
					$selected = ($county == $area[1]) ? 'selected="selected"' : '';
					$this->counties .= '<option value="'.$county.'" '.$selected.'>'.$county.'</option>';
				}

				foreach($this->towns_cities_array as $town_city)
				{
					$selected = ($town_city == $area[2]) ? 'selected="selected"' : '';
					$this->towns_cities .= '<option value="'.$town_city.'" '.$selected.'>'.$town_city.'</option>';
				}
			}
		}
		else
		{
			if (isset($this->areas))
			{
				// foreach ($this->areas as $region => $counties)
				// {
				// 	$this->regions .= '<option value="'.$region.'">'.$region.'</option>';
				// 	foreach($counties as $county => $towns_cities)
				// 	{
				// 		$this->counties .= '<option value="'.$county.'">'.$county.'</option>';
				// 		foreach($towns_cities as $town_city)
				// 		{
				// 			$this->towns_cities .= '<option value="'.$town_city.'">'.$town_city.'</option>';
				// 		}
				// 	}
				// }
				foreach($this->regions_array as $region)
				{
					$this->regions .= '<option value="'.$region.'" '.$selected.'>'.$region.'</option>';
				}

				foreach($this->counties_array as $county)
				{
					$this->counties .= '<option value="'.$county.'" '.$selected.'>'.$county.'</option>';
				}

				foreach($this->towns_cities_array as $town_city)
				{
					$this->towns_cities .= '<option value="'.$town_city.'" '.$selected.'>'.$town_city.'</option>';
				}
			}
		}
	}
	/**
	 * Sort Areas
	 * Breaks areas down into Region, County and Town/City arrays and sorts them in preparation for displaying.
	 */
	public function sort_areas()
	{
		foreach($this->areas as $region => $counties)
		{
			array_push($this->regions_array, $region);
			foreach($counties as $county => $town_cities)
			{
				array_push($this->counties_array, $county);
				foreach($town_cities as $town_city)
				{
					array_push($this->towns_cities_array, $town_city);
				}
			}
		}
		asort($this->regions_array, SORT_STRING);
		asort($this->counties_array, SORT_STRING);
		asort($this->towns_cities_array, SORT_STRING);
	}
}
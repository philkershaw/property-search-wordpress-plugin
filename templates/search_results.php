<?php get_header(); ?>

<?php
	$ts_property_layout = ts_get_option('ts_property_layout');
	$ts_property_search = ts_get_option('ts_property_search');

	$ts_measurement = apply_filters('ts_measurement_unit', ts_get_option('ts_measurement_unit'));
?>

<div id="main-wrap">

	<div id="main" class="clearfix">

		<?php

			global $wp_query;
			$s_false = false;

			// set custom post type
			$post_type = array('sale');

			// set orderby and order
			$orderby = ($search_get['orderby']=='price') ? 'meta_value_num' : 'date';
			$order = $search_get['order'] ? $search_get['order'] : 'DESC';

			// posts per page
			$posts_per_page = $search_get['nr'] ? $search_get['nr'] : get_option('posts_per_page');

			// check price custom fields
			if($_GET['s'])
			{
				$meta_query_location = array(
					'key' => 'btg_property_search_areas',
					'value' => $_GET['s'],
					'compare' => '=',
					'type' => 'char'
				);
			}
			if($_GET['btg_property_search_type'])
			{
				$meta_query_type = array(
					'key' => 'btg_property_search_type',
					'value' => $_GET['btg_property_search_type'],
					'compare' => '=',
					'type' => 'char'
				);
			}
			$meta_query = array();
			array_push($meta_query, $meta_query_location, $meta_query_type);

			$args = array(
				'post_type' => $post_type,
				'paged' => get_query_var('paged'),
				'orderby' => $orderby,
				'order' => $order
			);

			// add meta query
			if(!empty($meta_query))
				$args = array_merge($args, array('meta_query' => $meta_query));

		 	query_posts($args);
		 	get_template_part('properties');
		?>
	</div><!-- end main -->
</div><!-- end main-wrap -->
<?php get_footer(); ?>